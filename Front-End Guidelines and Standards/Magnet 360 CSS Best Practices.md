# CSS
### Reference: https://google.github.io/styleguide/htmlcssguide.html#CSS

### Use meaningful or generic ID and class names.
    /* Not recommended: meaningless */
    #yee-1901 {}

    /* Not recommended: presentational */
    .button-green {}
    .clear {}
    /* Recommended: specific */
    #gallery {}
    #login {}
    .video {}

    /* Recommended: generic */
    .aux {}
    .alt {}

### Use ID and class names that are as short as possible but as long as necessary.
    /* Not recommended */
    #navigation {}
    .atr {}
    /* Recommended */
    #nav {}
    .author {}

### Unless necessary (for example with helper classes), do not use element names in conjunction with IDs or classes.
    /* Not recommended */
    ul#example {}
    div.error {}
    /* Recommended */
    #example {}
    .error {}

### Use shorthand properties where possible.
    /* Not recommended */
    border-top-style: none;
    font-family: palatino, georgia, serif;
    font-size: 100%;
    line-height: 1.6;
    padding-bottom: 2em;
    padding-left: 1em;
    padding-right: 1em;
    padding-top: 0;
    /* Recommended */
    border-top: 0;
    font: 100%/1.6 palatino, georgia, serif;
    padding: 0 1em 2em;

### Do not use units after 0 values unless they are required.
    flex: 0px; /* This flex-basis component requires a unit. */
    flex: 1 1 0px; /* Not ambiguous without the unit, but needed in IE11. */
    margin: 0;
    padding: 0;

    Omit leading “0”s in values.
    font-size: .8em;

### Use 3 character hexadecimal notation where possible.
    /* Not recommended */
    color: #eebbcc;
    /* Recommended */
    color: #ebc;

### Prefix selectors with an application-specific prefix (optional).  In large projects as well as for code that gets embedded in other projects or on external sites use prefixes (as namespaces) for ID and class names. Use short, unique identifiers followed by a dash.
    .adw-help {} /* AdWords */
    #maia-note {} /* Maia */

### Separate words in ID and class names by a hyphen. (Exception: Use underscore in VisualForce pages.)
    /* Not recommended: does not separate the words “demo” and “image” */
    .demoimage {}

    /* Not recommended: uses underscore instead of hyphen */
    .error_status {}
    /* Recommended */
    #video-id {}
    .ads-sample {}
    
## CSS Formatting Rules
### Alphabetize declarations.
    background: fuchsia;
    border: 1px solid;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
    color: black;
    text-align: center;
    text-indent: 2em;

### Indent all block content.
    @media screen, projection {

      html {
        background: #fff;
        color: #444;
      }

    }

### End every declaration with a semicolon for consistency and extensibility reasons.
    /* Not recommended */
    .test {
      display: block;
      height: 100px
    }
    /* Recommended */
    .test {
      display: block;
      height: 100px;
    }

### Always use a single space between property and value (but no space between property and colon) for consistency reasons.
    /* Not recommended */
    h3 {
      font-weight:bold;
    }
    /* Recommended */
    h3 {
      font-weight: bold;
    }

### Use a space between the last selector and the declaration block.
    /* Not recommended: missing space */
    #video{
      margin-top: 1em;
    }

    /* Not recommended: unnecessary line break */
    #video
    {
      margin-top: 1em;
    }
    /* Recommended */
    #video {
      margin-top: 1em;
    }

### Separate selectors and declarations by new lines.
    /* Not recommended */
    a:focus, a:active {
      position: relative; top: 1px;
    }
    /* Recommended */
    h1,
    h2,
    h3 {
      font-weight: normal;
      line-height: 1.2;
    }

### Always put a blank line (two line breaks) between rules.
    html {
      background: #fff;
    }

    body {
      margin: auto;
      width: 50%;
    }

### Use single quotation marks for attribute selectors and property values.
    /* Not recommended */
    @import url("https://www.google.com/css/maia.css");

    html {
      font-family: "open sans", arial, sans-serif;
    }
    /* Recommended */
    @import url(https://www.google.com/css/maia.css);

    html {
      font-family: 'open sans', arial, sans-serif;
    }

