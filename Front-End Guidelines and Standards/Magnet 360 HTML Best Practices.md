# HTML
## Reference: https://google.github.io/styleguide/htmlcssguide.html

### Indent by 2 spaces at a time. Don’t use tabs or mix tabs and spaces for indentation.
    <ul>
      <li>Fantastic</li>
      <li>Great</li>
    </ul>
    .example {
      color: blue;
    }

### All code must be lowercase.
    <!-- Not recommended -->
    <A HREF="/">Home</A>
    <!-- Recommended -->
    <img src="google.png" alt="Google">
    /* Not recommended */
    color: #E5E5E5;
    /* Recommended */
    color: #e5e5e5;

    Remove trailing white space.
    <!-- Not recommended -->
    <p>What?_
    <!-- Recommended -->
    <p>Yes please.

### Make sure your editor uses UTF-8 as character encoding, without a byte order mark.  Specify the encoding in HTML templates and documents via <meta charset="utf-8">.
### Mark todos and action items with TODO.
### Append a contact (username or mailing list) in parentheses as with the format TODO(contact).
### Append action items after a colon as in TODO: action item.
    {# TODO(john.doe): revisit centering #}
    <center>Test</center>
    <!-- TODO: remove optional tags -->
    <ul>
      <li>Apples</li>
      <li>Oranges</li>
    </ul>

### Use HTML according to its purpose.
    <!-- Not recommended -->
    <div onclick="goToRecommendations();">All recommendations</div>
    <!-- Recommended -->
    <a href="recommendations/">All recommendations</a>

    Provide alternative contents for multimedia.
    <!-- Not recommended -->
    <img src="spreadsheet.png">
    <!-- Recommended -->
    <img src="spreadsheet.png" alt="Spreadsheet screenshot.">

### Strictly keep structure (markup), presentation (styling), and behavior (scripting) apart, and try to keep the interaction between the three to an absolute minimum.
    <!-- Not recommended -->
    <!DOCTYPE html>
    <title>HTML sucks</title>
    <link rel="stylesheet" href="base.css" media="screen">
    <link rel="stylesheet" href="grid.css" media="screen">
    <link rel="stylesheet" href="print.css" media="print">
    <h1 style="font-size: 1em;">HTML sucks</h1>
    <p>I’ve read about this on a few sites but now I’m sure:
      <u>HTML is stupid!!1</u>
    <center>I can’t believe there’s no way to control the styling of
      my website without doing everything all over again!</center>
    <!-- Recommended -->
    <!DOCTYPE html>
    <title>My first CSS-only redesign</title>
    <link rel="stylesheet" href="default.css">
    <h1>My first CSS-only redesign</h1>
    <p>I’ve read about this on a few sites but today I’m actually
      doing it: separating concerns and avoiding anything in the HTML of
      my website that is presentational.
    <p>It’s awesome!

### There is no need to use entity references like &mdash;, &rdquo;, or &#x263a;, assuming the same encoding (UTF-8) is used for files and editors as well as among teams.
    <!-- Not recommended -->
    The currency symbol for the Euro is &ldquo;&eur;&rdquo;.
    <!-- Recommended -->
    The currency symbol for the Euro is “€”.

## HTML Formatting Rules
#### Use a new line for every block, list, or table element, and indent every such child element.
    <blockquote>
      <p><em>Space</em>, the final frontier.</p>
    </blockquote>
    <ul>
      <li>Moe</li>
      <li>Larry</li>
      <li>Curly</li>
    </ul>
    <table>
      <thead>
        <tr>
          <th scope="col">Income</th>
          <th scope="col">Taxes</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>$ 5.00</td>
          <td>$ 4.50</td>
        </tr>
      </tbody>
    </table>

### Use double ("") rather than single quotation marks ('') around attribute values.
    <!-- Not recommended -->
    <a class='maia-button maia-button-secondary'>Sign in</a>
    <!-- Recommended -->
    <a class="maia-button maia-button-secondary">Sign in</a>

