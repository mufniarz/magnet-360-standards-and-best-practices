#Visualforce Pages 
##Know All Visualforce Tags 

###All developers (FE, BE) should be familiar with all Visualforce tags.  Using standard Salesforce components in your Visualforce pages allows us to use functionality without writing more code.   
###Most Visualforce tags allow you to specify styles and class names, so you can style any native component with a custom Look and Feel. 
###(!) Visualforce Developer’s Guide:  See “Standard Component Reference” 
###http://www.salesforce.com/us/developer/docs/pages/ 

##Know Visualforce Best Practices 
###Along with understanding all available Visualforce Components, review Salesforce’s Best Practices.   
###(!) Visualforce Developer’s Guide:  See “Best Practices” 
###http://www.salesforce.com/us/developer/docs/pages/ 

 

##Action Functions 
###Did I mention you need to know all Visualforce Tags?   
###In particular, the following tags are crucial in allowing Visualforce, Javascript and Apex to communicate and pass information between each other.   

`apex:actionFunction 
apex:actionPoller 
apex:actionRegion 
apex:actionStatus 
apex:actionSupport`

##Key Tags 
###If you don’t have time to review ALL Visualforce tags (which you should) - these are the KEY ones you should know inside/out. 

`apex:attribute 
apex:commandButton / apex:commandLink 
apex:detail 
apex:form 
apex:inputField 
apex:inputText 
apex:listViews 
apex:outputField 
apex:outputLabel 
apex:outputPanel 
apex:outputText 
apex:pageBlock / apex:pageBlockSection / apex:pageBlockTable 
apex:pageMessage / apex:pageMessages 
apex:relatedList 
apex:repeat 
apex:variable`

###(!) Visualforce Developer’s Guide:  See “Standard Component Reference” 
###http://www.salesforce.com/us/developer/docs/pages/ 