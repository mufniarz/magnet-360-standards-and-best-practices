# General
## General Style Rules

### Use the HTTPS protocol for embedded resources where possible.
	<!-- Not recommended: omits the protocol -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<!-- Not recommended: uses the HTTP protocol -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<!-- Recommended -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	/* Not recommended: omits the protocol */
	@import '//fonts.googleapis.com/css?family=Open+Sans';

	/* Not recommended: uses the HTTP protocol */
	@import 'http://fonts.googleapis.com/css?family=Open+Sans';
	/* Recommended */
	@import 'https://fonts.googleapis.com/css?family=Open+Sans';
		
### Indent by 2 spaces at a time.
	<ul>
	  <li>Fantastic
	  <li>Great
	</ul>
	.example {
	  color: blue;
	}
